package main

import (
	"fmt"
	"net/http"
	"os"
	"testing"
	"time"

	"urlshortener.fmac.com/dao"
	"urlshortener.fmac.com/db/postgresql"
	"urlshortener.fmac.com/db/redis"
	"urlshortener.fmac.com/model"
	"urlshortener.fmac.com/service/cache"
	"urlshortener.fmac.com/service/config"
	"urlshortener.fmac.com/service/metric"
	shortener "urlshortener.fmac.com/service/shorter"
)

func TestMain(m *testing.M) {
	config.GetInstance().TestMode = true
	metric.GetMetric().Init()
	i := m.Run()
	os.Exit(i)
}

func TestEncodeBase62(t *testing.T) {
	t.Logf("Iniciando test de encoding en base 62")
	r := shortener.EncodeShortUrl(4654456456456650001)
	if r != "5XpSW39NB1l" {
		t.Errorf("Error. esparaba el valor 5XpSW39NB1l y el metodo devolvio: %s", r)
	} else {
		t.Log("Finalizó el test de enonding")
	}

}

func TestDecodeBase62(t *testing.T) {
	t.Logf("Iniciando test de decode en base 62")
	r, err := shortener.DecodeShortUrl("5XpSW39NB1l")
	if err != nil {
		t.Error("El metodo devolvio error", err)
	} else {
		if r != 4654456456456650001 {
			t.Errorf("Error. esparaba el valor 4654456456456650001 y el metodo devolvio: %d", r)
		}
	}
}

func TestEncodeDecodeBase62(t *testing.T) {
	t.Logf("Iniciando test de encode y decode en base 62")
	timestamp := time.Now().UnixNano()
	base62 := shortener.EncodeShortUrl(timestamp)
	r, err := shortener.DecodeShortUrl(base62)
	if err != nil {
		t.Error("El metodo devolvio error", err)
	} else {
		if r != timestamp {
			t.Errorf("Error. esparaba el valor %d y el metodo devolvio: %d", timestamp, r)
		}
	}
}

func TestCreateShortenerUri(t *testing.T) {
	dto := &model.ShortenerUrl{Acitvo: true, TargetUrl: "http://estoEsUnaPrueba", Email: "NONO"}
	dto, err := shortener.GetInstance().CreateShortener(dto)
	if err != nil {
		t.Error("El metodo devolvio error", err)
	}
	if dto.Id != 20 {
		t.Error("La db mockeada siempre devuelve 20 de id.")
	}
	if dto.ShortenerUri != "K" {
		t.Errorf("El servicio devolvio mal la uri corta. Valor deveuelto %s", dto.ShortenerUri)
	}
}

func TestGetTargetUrl(t *testing.T) {
	dto, err := shortener.GetInstance().GetTargetUrl("K")
	if err != nil {
		t.Error("El metodo devolvio error", err)
	}
	if dto.TargetUrl != "http://estoEsUnaPrueba" {
		t.Errorf("El servicio, no devuelve la url correcta. Valor devuelo %s, esperado http://estoEsUnaPrueba", dto.TargetUrl)
	}
}

func TestUpdateTargetUrl(t *testing.T) {
	dto, err := shortener.GetInstance().FindByCode("K")
	if err != nil {
		t.Error("El metodo devolvio error", err)
	}
	dto.TargetUrl = "http://urlNueva"
	err = shortener.GetInstance().Update(dto.ShortenerUri, dto)
	if err != nil {
		t.Error("El metodo devolvio error", err)
	}
	dto, err = shortener.GetInstance().FindByCode("K")
	if err != nil {
		t.Error("El metodo devolvio error", err)
	}
	if dto.TargetUrl != "http://urlNueva" {
		t.Errorf("El servicio, no actualiza la url correcta. Valor devuelto %s, esperado http://urlNueva", dto.TargetUrl)
	}
}

func TestGetTargetInactiveUrl(t *testing.T) {
	dto, err := shortener.GetInstance().FindByCode("K")
	if err != nil {
		t.Error("El metodo devolvio error", err)
	}
	dto.Acitvo = false
	err = shortener.GetInstance().Update(dto.ShortenerUri, dto)
	if err != nil {
		t.Error("El metodo devolvio error", err)
	}
	dto, err = shortener.GetInstance().GetTargetUrl("K")
	if err != nil {
		t.Error("El metodo devolvio error", err)
	}
	if dto != nil {
		t.Error("El servicio no inactivo la url")
	}
}

func TestCheckChangeShortUri(t *testing.T) {
	dto, err := shortener.GetInstance().FindByCode("K")
	if err != nil {
		t.Error("El metodo devolvio error", err)
	}
	dto.ShortenerUri = "Jk"
	err = shortener.GetInstance().Update(dto.ShortenerUri, dto)
	if err == nil {
		t.Errorf("El servicio permitio cambiar la url corta y no está permitido.")
	}
}

func TestMetricGauge(t *testing.T) {
	metric.GetMetric().SetGaugeValue("Test", "Test", 256)
	metric.GetMetric().SetGaugeValue("Test", "Test2", 300)
	r := metric.GetMetric().GetGaugeValue("Test", "Test")
	if r != 256 {
		t.Errorf("La metrico devolvio un valor incorrecto. Valor devuelto %f, esperado 256 ", r)
	}
	r = metric.GetMetric().GetGaugeValue("Test", "Test2")
	if r != 300 {
		t.Errorf("La metrico devolvio un valor incorrecto. Valor devuelto %f, esperado 300 ", r)
	}
}

func TestMetricCounter(t *testing.T) {
	metric.GetMetric().CounterIncrement("requestCount", "ShortUri", "TargetUrl", "Jlkjlk", "http/dir1")
	metric.GetMetric().CounterIncrement("requestCount", "ShortUri", "TargetUrl", "Jlkjlk", "http/dir1")
	metric.GetMetric().CounterIncrement("requestCount", "ShortUri", "TargetUrl", "Jlkjlk", "http/dir1")
	metric.GetMetric().CounterIncrement("requestCount", "ShortUri", "TargetUrl", "Jlkjlk", "http/dir1")
	metric.GetMetric().CounterIncrement("requestBad", "ShortUri", "TargetUrl", "xx", "")
	metric.GetMetric().CounterIncrement("requestBad", "ShortUri", "TargetUrl", "xx", "")
	r := metric.GetMetric().GetCounterValue("requestCount", "ShortUri", "TargetUrl", "Jlkjlk", "http/dir1")
	if r != 4 {
		t.Errorf("La metrico devolvio un valor incorrecto. Valor devuelto %f, esperado 4 ", r)
	}
	r = metric.GetMetric().GetCounterValue("requestBad", "ShortUri", "TargetUrl", "xx", "")
	if r != 2 {
		t.Errorf("La metrico devolvio un valor incorrecto. Valor devuelto %f, esperado 2 ", r)
	}
}

/******APARTIR DE ACÁ, LOS TEST REQUIEREN DE UNA CONEXIÓN A UNA BASE DE POSTGRESQL Y REDIS****/

func TestCacheRedisReadWrite(t *testing.T) {
	redis.Init()
	config.GetInstance().TestMode = false //pongo el testmode en false, para que levanta las conexiones a la bd.
	dto := model.ShortenerUrl{Id: 44, Acitvo: true, TargetUrl: "kjhkjh"}
	err := cache.GetCache().Write("Key1", &dto, time.Now().Add(time.Minute*5))
	if err != nil {
		t.Error("El metodo devolvio error", err)
	}
	dto1 := model.ShortenerUrl{Id: 45, Acitvo: true, TargetUrl: "ssss"}
	err = cache.GetCache().Write("Key2", &dto1, time.Now().Add(time.Minute*5))
	if err != nil {
		t.Error("El metodo devolvio error", err)
	}

	r := &model.ShortenerUrl{}
	_, err = cache.GetCache().Read("Key1", r)
	if err != nil {
		t.Error("El metodo devolvio error", err)
	}
	if r.Id != dto.Id {
		t.Error("El caché no devolvio el valor esperado")
	}

	_, err = cache.GetCache().Read("Key2", r)
	if err != nil {
		t.Error("El metodo devolvio error", err)
	}
	if r.Id != dto1.Id {
		t.Error("El caché no devolvio el valor esperado")
	}
}

func TestCacheExistsDelete(t *testing.T) {
	if err := cache.GetCache().Delete("Key1"); err != nil {
		t.Error("El metodo devolvio error", err)
	}
	if err := cache.GetCache().Delete("Key2"); err != nil {
		t.Error("El metodo devolvio error", err)
	}
	if exist := cache.GetCache().Exists("Key1"); exist {
		t.Error("El caché no eliminó a la clave key1")
	}

	if exist := cache.GetCache().Exists("Key2"); exist {
		t.Error("El caché no eliminó a la clave key2")
	}

}
func TestDaoWrite(t *testing.T) {
	postgresql.GetPostgreSql().Init()
	dto := &model.ShortenerUrl{Acitvo: true, TargetUrl: "http://estoEsUnaPrueba", Email: "NONO", ShortenerUri: "EstoEsUnCodigoPrueba"}
	if r, err := dao.GetShortenerDao().Insert(dto); err != nil || r.Id == 0 {
		t.Error("Error al insertar una entidad", err)
	}
}

func TestDaoRead(t *testing.T) {
	dto, err := dao.GetShortenerDao().FindByCode("EstoEsUnCodigoPrueba")
	if err != nil {
		t.Error("Error al recuperar una entidad", err)
	}
	if dto == nil || dto.ShortenerUri != "EstoEsUnCodigoPrueba" {
		t.Error("Error. La entidad recuperada, no coincide con lo esperado.", err)
	}
}

func TestDaoDelete(t *testing.T) {
	err := dao.GetShortenerDao().Delete("EstoEsUnCodigoPrueba")
	if err != nil {
		t.Error("Error al eliminar una entidad", err)
	}
	dto, err := dao.GetShortenerDao().FindByCode("EstoEsUnCodigoPrueba")
	if err != nil {
		t.Error("Error al recuperar una entidad", err)
	}
	if dto != nil {
		t.Error("Error. La entidad No se eliminó.", err)
	}
}

func TestApiStressRequest(t *testing.T) {
	config.GetInstance().TestMode = false
	go main()
	time.Sleep(2 * time.Second)

	start := time.Now()
	i := 0

	for time.Until(start).Seconds() > -60 {
		fmt.Printf("%f", time.Until(start).Seconds())
		i++
		url := "http://localhost:8080/m"

		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Error("Error", err)
		}
		client := &http.Client{CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		}}

		resp, err := client.Do(req)
		if err != nil {
			t.Error("Error", err)
		}
		resp.Body.Close()
	}
	if i < 500 {
		t.Errorf("El api no superó los 500 rpm. Alcanzados %d", i)
	} else {
		fmt.Printf("se ejecutaron %d redirect al api", i)
	}
}
