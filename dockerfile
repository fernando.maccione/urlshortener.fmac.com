FROM alpine 
ADD application /
ADD server.yml /

WORKDIR /
CMD ["/application"]
EXPOSE 8080
