package dao

import (
	"database/sql"
	"errors"
	"strconv"
	"strings"

	"urlshortener.fmac.com/db/postgresql"
	"urlshortener.fmac.com/model"
	"urlshortener.fmac.com/service/config"
)

type ShortenerDao interface {
	Update(dto *model.ShortenerUrl) error
	Insert(dto *model.ShortenerUrl) (*model.ShortenerUrl, error)
	FindById(int64) (*model.ShortenerUrl, error)
	FindByCode(string) (*model.ShortenerUrl, error)
	Delete(code string) error
}

type shortenerDaoImp struct{}

func GetShortenerDao() ShortenerDao {
	if !config.GetInstance().TestMode {
		return shortenerDaoImp{}
	} else {
		return shortenerDAOTESTImp{}
	}

}

func (c shortenerDaoImp) Update(dto *model.ShortenerUrl) error {
	db, err := postgresql.GetPostgreSql().Connect()
	if err != nil {
		return err
	}
	defer db.Close()
	if err := checkId(strconv.FormatInt(dto.Id, 10)); err != nil {
		return err
	}
	sqlStatement := `UPDATE shortenerurl set ShortenerUrl = $1, TargetUrl = $2, email=$3, activo = $4 where id = $5`

	if _, err := db.Exec(sqlStatement, dto.ShortenerUri, dto.TargetUrl, dto.Email, dto.Acitvo, dto.Id); err != nil {
		return err
	}
	return nil
}

func (c shortenerDaoImp) Insert(dto *model.ShortenerUrl) (*model.ShortenerUrl, error) {
	db, err := postgresql.GetPostgreSql().Connect()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	sqlStatement := `INSERT INTO shortenerurl (ShortenerUrl, TargetUrl, email, activo)
		VALUES ($1, $2, $3, $4)
		RETURNING id`

	var id int64 = 0

	if err := db.QueryRow(sqlStatement, dto.ShortenerUri, dto.TargetUrl, dto.Email, dto.Acitvo).Scan(&id); err != nil {
		return nil, err
	}

	dto.Id = id
	return dto, nil
}

func (c shortenerDaoImp) FindByCode(shortUrl string) (*model.ShortenerUrl, error) {
	db, err := postgresql.GetPostgreSql().Connect()
	if err != nil {
		return nil, nil
	}
	defer db.Close()
	if err := checkId(shortUrl); err != nil {
		return nil, err
	}
	sqlStatement := `select id, ShortenerUrl, TargetUrl, email, activo from shortenerurl where ShortenerUrl = $1`
	dto := &model.ShortenerUrl{}

	row := db.QueryRow(sqlStatement, shortUrl)
	switch err := row.Scan(&dto.Id, &dto.ShortenerUri, &dto.TargetUrl, &dto.Email, &dto.Acitvo); err {
	case sql.ErrNoRows:
		return nil, nil
	case nil:
		return dto, nil
	default:
		return nil, err
	}
}

func (c shortenerDaoImp) FindById(int64) (*model.ShortenerUrl, error) {
	return nil, nil
}

func (c shortenerDaoImp) Delete(code string) error {
	db, err := postgresql.GetPostgreSql().Connect()
	if err != nil {
		return err
	}
	defer db.Close()
	if err := checkId(code); err != nil {
		return err
	}
	sqlStatement := `DELETE FROM shortenerurl WHERE ShortenerUrl = $1`

	if _, err := db.Exec(sqlStatement, code); err != nil {
		return err
	}
	return nil
}

func checkId(value string) error {
	permitedCharacter := "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	for _, s := range value {
		if !strings.Contains(permitedCharacter, string(s)) {
			return errors.New("invalid character")
		}
	}
	return nil
}

/***Implementación para test*****/

var dtoTest *model.ShortenerUrl

type shortenerDAOTESTImp struct{}

func (c shortenerDAOTESTImp) Update(dto *model.ShortenerUrl) error {
	dtoTest = dto
	return nil
}
func (c shortenerDAOTESTImp) Insert(dto *model.ShortenerUrl) (*model.ShortenerUrl, error) {
	dtoTest = dto
	dtoTest.Id = 20
	return dtoTest, nil
}
func (c shortenerDAOTESTImp) FindById(id int64) (*model.ShortenerUrl, error) {
	return dtoTest, nil
}
func (c shortenerDAOTESTImp) FindByCode(uri string) (*model.ShortenerUrl, error) {
	if dtoTest != nil && dtoTest.ShortenerUri == uri {
		return dtoTest, nil
	} else {
		return nil, nil
	}
}
func (c shortenerDAOTESTImp) Delete(code string) error {
	if dtoTest.ShortenerUri == code {
		dtoTest = nil
	}
	return nil
}
