package web

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"urlshortener.fmac.com/service/metric"
	"urlshortener.fmac.com/web/admin"
	server "urlshortener.fmac.com/web/serverShortener"
)

/*Me falta agregarle un recover para capturar los panic y algun json para devolver errores*/
func Register() {
	router := gin.Default()
	gin.SetMode(gin.DebugMode)
	admin.Register(router)
	server.Register(router)

	/*Metricas*/
	router.GET("/admin/ping", handlePing)
	router.GET("/admin/metric", handleMetric)

	router.Run(":8080")

}

func handlePing(c *gin.Context) {
	defer recoverPanic(c)
	c.String(http.StatusOK, "Yo estoy de lujo!")
}

func handleMetric(c *gin.Context) {
	defer recoverPanic(c)
	h := metric.GetMetric().Handler()
	h.ServeHTTP(c.Writer, c.Request)
}

func recoverPanic(c *gin.Context) {
	recuperado := recover()
	if recuperado != nil {
		str := fmt.Sprintf("%v", recuperado)
		c.String(http.StatusInternalServerError, str)
	}
}
