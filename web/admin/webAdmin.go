package admin

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"urlshortener.fmac.com/model"
	"urlshortener.fmac.com/service/metric"
	shortener "urlshortener.fmac.com/service/shorter"
)

func Register(router *gin.Engine) {
	basePath := "/admin/api/urlshortener"
	router.POST(basePath, handlePost)
	router.GET(basePath+"/:urlshortener", handleGet)
	router.PUT(basePath+"/:urlshortener", handlePut)
	router.DELETE(basePath+"/:urlshortener", handleDelete)

	/*Documentación openApi*/
	router.StaticFS("/admin/api/swagger/", http.Dir("./swagger/dist/"))
	router.GET("/admin/api/swagger", handleSwagger)
}

func handleGet(c *gin.Context) {
	defer recoverPanic(c)
	url := c.Param("urlshortener")

	dto, err := shortener.GetInstance().FindByCode(url)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
	} else {
		if dto != nil {
			dto.ShortenerUrl = `http://` + c.Request.Host + `/` + dto.ShortenerUri
			c.JSON(http.StatusOK, dto)
		} else {
			c.String(http.StatusNotFound, "Not Found")
		}

	}

}

func handlePost(c *gin.Context) {
	defer recoverPanic(c)
	body := c.Request.Body
	dto := &model.ShortenerUrl{}
	err := json.NewDecoder(body).Decode(dto)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
	} else {
		dto, err = shortener.GetInstance().CreateShortener(dto)
		if err != nil {
			c.JSON(http.StatusInternalServerError, err.Error())
		} else {
			metric.GetMetric().CounterIncrement("geneatedNewUrl", "ShortUri", "user", dto.ShortenerUri, dto.Email)
			dto.ShortenerUrl = `http://` + c.Request.Host + `/` + dto.ShortenerUri
			c.JSON(http.StatusOK, dto)
		}
	}

}

func handlePut(c *gin.Context) {
	defer recoverPanic(c)
	body := c.Request.Body
	shortUrl := c.Param("urlshortener")
	dto := &model.ShortenerUrl{}
	err := json.NewDecoder(body).Decode(dto)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
	} else {
		err := shortener.GetInstance().Update(shortUrl, dto)
		if err != nil {
			c.JSON(http.StatusInternalServerError, err.Error())
		} else {
			dto.ShortenerUrl = `http://` + c.Request.Host + `/` + dto.ShortenerUri
			metric.GetMetric().CounterIncrement("modifiedUrl", "ShortUri", "user", dto.ShortenerUri, dto.Email)
			c.JSON(http.StatusOK, dto)
		}
	}

}

func handleDelete(c *gin.Context) {
	defer recoverPanic(c)
	url := c.Param("urlshortener")
	err := shortener.GetInstance().DeleteByShortenerUrl(url)
	if err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
	} else {
		c.String(http.StatusOK, "")
	}
}
func recoverPanic(c *gin.Context) {
	recuperado := recover()
	if recuperado != nil {
		str := fmt.Sprintf("%v", recuperado)
		c.String(http.StatusInternalServerError, str)
	}
}

func handleSwagger(c *gin.Context) {
	c.Redirect(http.StatusPermanentRedirect, `http://`+c.Request.Host+`/admin/api/swagger/index.html`)
}
