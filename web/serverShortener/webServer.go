package server

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"urlshortener.fmac.com/model"
	"urlshortener.fmac.com/service/metric"
	shortener "urlshortener.fmac.com/service/shorter"
)

func Register(router *gin.Engine) {
	router.GET("/:urlshorter", handleRedirectUrl)
}

func handleRedirectUrl(c *gin.Context) {
	defer recoverPanic(c)
	start := time.Now()
	uri := c.Param("urlshorter")
	uDto, err := shortener.GetInstance().GetTargetUrl(uri)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
	} else {
		if uDto != nil {
			if `http://`+c.Request.Host+c.Request.RequestURI == uDto.TargetUrl {
				c.String(http.StatusBadRequest, "Recursión no permitida")
			} else {
				endTime := time.Now()
				go auditRequetOk(uDto, float64(endTime.Sub(start).Microseconds())/1000)
				c.Redirect(http.StatusTemporaryRedirect, uDto.TargetUrl)
			}
		} else {
			metric.GetMetric().CounterIncrement("requestError", "ShortUri", "TargetUrl", uri, "")
			c.String(http.StatusNotFound, "Url no disponible")
		}
	}
}

func auditRequetOk(uDto *model.ShortenerUrl, time float64) {
	metric.GetMetric().CounterIncrement("requestCount", "ShortUri", "TargetUrl", uDto.ShortenerUri, uDto.TargetUrl)
	metric.GetMetric().CounterIncrement("requestCountUser", "Name", "email", "", uDto.Email)
	metric.GetMetric().SetGaugeValue("timeResponseMiliSec", uDto.ShortenerUri, time)
}

func recoverPanic(c *gin.Context) {
	recuperado := recover()
	if recuperado != nil {
		str := fmt.Sprintf("%v", recuperado)
		c.String(http.StatusInternalServerError, str)
	}
}
