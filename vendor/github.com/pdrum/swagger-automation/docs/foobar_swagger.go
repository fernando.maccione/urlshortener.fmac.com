package docs

import "github.com/pdrum/swagger-automation/api"

type foobarResponseWrapper struct {
	Body api.FooBarResponse
}

type foobarParamsWrapper struct {
	Body api.FooBarRequest
}
