# Shortener Api
Servicio para suministrar url cortas para poder utilizar en twitter

## Instalacion

```
cd $GO_HOME/src
git clone https://gitlab.com/fernando.maccione/urlshortener.fmac.com.git
```

## Ejecutar test
```
go test  --cover
(la ejecución de algunos test, requieren de la conexión de una base postgres y redis)
```

## compilación 
```
go build
```

## compilación (version Docker)
```
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o application .
```

## Instalar en AWS
```
./ShortenerUrl-AWS_UPDATE_PRODUCCION.sh
```

## Link a la documentación del Api en swagger:
http://ec2-54-144-66-165.compute-1.amazonaws.com:8080/admin/api/swagger/

## Tecnologias utilizadas

-Lenguaje de desarrollo:
    El api está escrita en golang. Se prefirió por sobre java, ya que no se requiere de un complejo modelado de objetos y se necesita un alto rendimiento de  la misma, con un bajo consumo de hardware. 

-Base de datos:<br>
    Postgresql:
    Mi primera opción fue utilizar algún key-value como dynamodb porque no tengo un mdelo de tablas complejos ni la la necesidad de mantener una integridad relacional entre tablas, pero al final opté por postgresql, montado sobre el servicio de RDS de aws, debido a que de esta forma con solo usar el id autonumerado de la tabla, ya  me alcanza y sobra para generar y garantizarme de obteber una clave corta de url, que no se repita.<br>
    Redis: Para incrementar el rendimiento, opté por poner un caché en redis. Se  podría utilizar el servicio de ElasticCache for Redis de aws.<br>

-Generacion de clave corta de url:<br>
    Pk autogenerado de la bd, con base en 62. Evalué usar timestamp+randon o uui, pero por mas que pase de base10 a 62, sigue siendo un valor largo como para usarlo de url corta. Por eso me quedé con el id. <br>

-Metricas:
    Prometheus. En este punto no evalué otras alternativas. Lo estamos usando en el trabajo y anda muy bien. Igualmente, el servicio está encapsulado con una interfaz, se podría implementar la metrica con otro proveedor sin problemas.

-Documentación del API:
    Swagger.

## Diseño del api

<p>Basicamente el diseño del api, se puede separar en tres partes.</p>
-     Un controlador web<br>
-     Una capa de servicios<br>
-     Una capa de acceso a datos.<br>

<br>
<p>Controlador web:</p>

<p>Basicamente expone mediante un api REST, los servicios del shortener. Esta compuesta por dos partes, una que expone los servicios para administrar las url cortas (post, put, delete, get), y otra para exponer al servicio que devuelve el redirect al destino. Esta ultima devuelve un 307 (redirect temporal), para que el navegador no cachee la direacción y esta se pueda inhabilitar o cambiar de destino y que surga efecto, por mas que un usuario ya haya accedido desde su navegador.<br>
El redirect, está separado del servicio de administración, para poder tenerlo en una instancia distinta con otras politicas de escalado. Se supone que es donde vas tráfico va a haber.</p>

<p>Capa de servicios:</p>
Acá están todos los servicios que necesita el api para funcionar.<br>
<p>-Shortener: Es el core del api. Este se encarga de administrar las url, generar la url corta, chequear la integridad de la misma cuando se hace alguna actualización. Existe como una especie de "contrato" entre la uri corta generada y el id de la entidad, ya que la uri es el id con la base cambiada a 62 (letras y numeros), para que queda aún mas corta. Entonces, para garantizar que el serivicio nunca devuelva una uri ya existente, a nivel de base de datos se colocó un constrain para asegurar que esto no suceda y ademas, el servicio cuando se hace una actualización, chequea que la uri vuelta pasar a base10, siga siendo igual al id, para que nadie la cambie intencionalmente.</p>

<p>-Cache: Hay disponible un servicio de cache para evitar ir concurrentemente a la base de datos y perder tiempo. Internamente implementa un redis, pero se pueden crear otras implementaciópn, como un caché interno.</p>

</p>-Metric: hay disponible un servicio de metricas, para auditar lo que necesite. Expone una interfaz por la cual te permite llevar contadores o indicadores estaticos. Actualmente se utilizan para auditar el tiempo que tarda en hacer un redirect y cuantas se piden. Internamente implementa prometheus, pero de igual manera que el resto de los servicios, se cambiar la implementación.</p>
<br>
<br>
</p>-Capa de acceso a datos:<br>
Como bien lo dice el nombre, acá se va a econtrar todo lo que se necesite para manejar la base de datos. Como el driver para acceder a la base de datos y los helper para persistir las entidades.<br>
Wrapper redis: Encapsula el driver para poder acceder a la base de redis.<br>
Wrapper Postgres: Encapsula la conexión a la base de datos de postgresql.<br>
DAO: Actualmente uno solo. Se encarga de persistir, por medio del wrapper postgres, la entidad urlshortener.</p>
<br>

![Diagrama del api](ApiShortener.png)<br><br>
Link al gráfico: https://miro.com/app/board/o9J_lxnTMq4=/

## Infraestructura

</p>Actualmente se encuentra hosteado en amazon, con dos instancias de EC2, una para redis y otra para hostear la aplicación. Las intancias son T2.micro, un nucleo con un 1 gb de ram. Y la base de datos se encuentra en el servicio de RDS.<br>
Pero idealmente, se podría utilizar un cluster de ECS (elastic container service), con dos servicios (uno para la administraciń de las url y otro para resolver las url cortas). Ambos servicios contarían un su respectivo task definition y serían del tipo Fargete, así nos abtraemos de mantener las instancias de Ec2 que crea el servicio común.<br>Ambas task, podrían compartir la misma imagen de docker, pero deberían presentar variables de entornos diferentes, para que en un servicio se levante solo la parte web de la administración de de las url y en el otro el redirect<br>
Servicios:<br>
AdminShortener: ASG configurado con 2 replicas<br>
ServerShortener: ASG configurado con minimo 2 replicas, deseado 5 (según pruebas que hice en mi entorno local... no estoy seguro de que sea así.) Maximo 10.<br><br>
Como punto entrada podemos utilizar un ELB (elastic loag balancer), configurado con dos reglas de enrutamiento:<br>
-/admin/api/* Debería apuntar al targeGruop del servicio AdminShortener.<br>
-/* Debería apuntar al targetGroup del servicio ServerAdmin<br>
</p>
<p>Adicionalmente, tambien podemos contar con ec2 para Prometheus, configurado con un scrape del tipo dinamico y tambien podemos contar con un ec2 para levantar un jenkins para devops.</p>
Todo el manejo de logs, se manejaría atraves del cloudwatch. Este servicio captura automaticamente todo lo que salga por consola de los containers.
![Diagrama del api](AWS-ShortenerURL.svg)
