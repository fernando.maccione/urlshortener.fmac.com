#!/usr/bin/env bash
#
# DESCRIPTION: ECS Deployment Script
# MAINTAINER: Fernando maccione
#

export PATH=~/.local/bin:$PATH

set -e

# BEGIN CUSTOMIZATIONS #
ECS_REGION='us-east-1'
ECS_CLUSTER_NAME='UrlShortener'
ECS_SERVICE_NAME='SRV-UrlShortenerAdmin'
ECS_TASK_DEFINITION_NAME='UrlShortenerAdmin-PRODUCTION'
ECR_NAME='fmac/Urlshortener'
ECR_URI='********.dkr.ecr.us-east-1.amazonaws.com'
VERSION=$(date +%s)
AWSCLI_VER_TAR=1.11.91
# END CUSTOMIZATIONS #

# BEGIN OTHER VAR DEFINITIONS #
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ORIGINAL_BRANCH="$(git rev-parse --abbrev-ref HEAD)"
ENVIRONMENT=""
BRANCH=""
AWSCLI_VER=$(aws --version 2>&1 | cut -d ' ' -f 1 | cut -d '/' -f 2)
# END OTHER VAR DEFINITIONS #

if [[ ${AWSCLI_VER} < ${AWSCLI_VER_TAR} ]]
then echo "ERROR: Please upgrade your AWS CLI to version ${AWSCLI_VER_TAR} or later!"
  exit 1
fi


ENVIRONMENT="PRODUCTION"
BRANCH=master


echo "You are deploying ${BRANCH} to ${ENVIRONMENT}."

# Git operations
(

  # Fetch latest from git origin
  git fetch --all

  # Checkout the git branch
  git checkout "${BRANCH}"
  
)

#Compilo la ultima version
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o application . 


DOCKERIMAGENAME="urlshortener-production"
# Docker operations
(
    
  # Build the Docker image (to do asset and template compilation, etc.)
  docker build -t "${DOCKERIMAGENAME}:latest" -f dockerfile .

  # Tag the new Docker image to the remote repo (by date)
  docker tag "${DOCKERIMAGENAME}:latest" "${ECR_URI}/${ECR_NAME}:${DOCKERIMAGENAME}-${VERSION}"

  # Login to ECR us-east-1
  $(aws ecr get-login --region "${ECS_REGION}" --no-include-email)

  # Push to the remote repo (by date)
  docker push "${ECR_URI}/${ECR_NAME}:${DOCKERIMAGENAME}-${VERSION}"

)

#Deploy del servicio de admin
ECS_SERVICE_NAME='SRV-UrlShortenerAdmin'
ECS_TASK_DEFINITION_NAME='UrlShortenerAdmin-PRODUCTION'
# ECS operations
(
  
  # Store revision
  REVISION=$(git rev-parse "${BRANCH}")

  # Get previous task definition from ECS
  PREVIOUS_TASK_DEF=$(aws ecs describe-task-definition --region "${ECS_REGION}" --task-definition "${ECS_TASK_DEFINITION_NAME}")

  # Create the new ECS container definition from the last task definition
  NEW_CONTAINER_DEF=$(echo "${PREVIOUS_TASK_DEF}" | python <(cat <<-EOF
import sys, json
definition = json.load(sys.stdin)['taskDefinition']['containerDefinitions']
definition[0]['image'] = '${ECR_URI}/${ECR_NAME}:${DOCKERIMAGENAME}-${VERSION}'
print json.dumps(definition)
EOF
  ))

  # Create the new task definition
  aws ecs register-task-definition --region "${ECS_REGION}" --family "${ECS_TASK_DEFINITION_NAME}" --container-definitions "${NEW_CONTAINER_DEF}" --network-mode "awsvpc" --task-role-arn "arn:aws:iam::*******:role/ecsTaskExecutionRole" --execution-role-arn "arn:aws:iam::*******:role/ecsTaskExecutionRole" --requires-compatibilities "FARGATE" --cpu "512" --memory "1024"

  # Update the service to use the new task defintion
  aws ecs update-service --region "${ECS_REGION}" --cluster "${ECS_CLUSTER_NAME}" --service "${ECS_SERVICE_NAME}" --task-definition "${ECS_TASK_DEFINITION_NAME}"


)

#Deploy del servicio de admin
ECS_SERVICE_NAME='SRV-UrlShortenerServer'
ECS_TASK_DEFINITION_NAME='UrlShortenerServer-PRODUCTION'
# ECS operations
(
  
  # Store revision
  REVISION=$(git rev-parse "${BRANCH}")

  # Get previous task definition from ECS
  PREVIOUS_TASK_DEF=$(aws ecs describe-task-definition --region "${ECS_REGION}" --task-definition "${ECS_TASK_DEFINITION_NAME}")

  # Create the new ECS container definition from the last task definition
  NEW_CONTAINER_DEF=$(echo "${PREVIOUS_TASK_DEF}" | python <(cat <<-EOF
import sys, json
definition = json.load(sys.stdin)['taskDefinition']['containerDefinitions']
definition[0]['image'] = '${ECR_URI}/${ECR_NAME}:${DOCKERIMAGENAME}-${VERSION}'
print json.dumps(definition)
EOF
  ))

  # Create the new task definition
  aws ecs register-task-definition --region "${ECS_REGION}" --family "${ECS_TASK_DEFINITION_NAME}" --container-definitions "${NEW_CONTAINER_DEF}" --network-mode "awsvpc" --task-role-arn "arn:aws:iam::*******:role/ecsTaskExecutionRole" --execution-role-arn "arn:aws:iam::*****:role/ecsTaskExecutionRole" --requires-compatibilities "FARGATE" --cpu "512" --memory "1024"

  # Update the service to use the new task defintion
  aws ecs update-service --region "${ECS_REGION}" --cluster "${ECS_CLUSTER_NAME}" --service "${ECS_SERVICE_NAME}" --task-definition "${ECS_TASK_DEFINITION_NAME}"


)


