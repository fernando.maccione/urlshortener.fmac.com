package postgresql

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
	"urlshortener.fmac.com/service/config"
)

type PostgreSql interface {
	Init() error
	Connect() (*sql.DB, error)
	Close() error
}

/*
Como no estoy manejando un poll de conexiones y no encontré un lib que ya lo haga para postgresql, por ahora, devulvo una instancia nueva. No hay tiempo..
*/
//var instance PostgreSql
func GetPostgreSql() PostgreSql {
	return &postgreSqlImp{}
}

type postgreSqlImp struct {
	db *sql.DB
}

func (c *postgreSqlImp) Init() error {
	if _, err := c.Connect(); err != nil {
		return err
	}
	if err := c.db.Ping(); err != nil {
		return err
	}
	fmt.Println("Successfully connected!")
	return c.Close()
}

func (c *postgreSqlImp) Connect() (*sql.DB, error) {
	config := config.GetInstance()
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		config.Postgres.Host, config.Postgres.Port, config.Postgres.User, config.Postgres.Password, config.Postgres.DataBase)

	var err error
	c.db, err = sql.Open("postgres", psqlInfo)

	if err != nil {
		return nil, err
	}
	return c.db, nil
}

func (c *postgreSqlImp) Close() error {
	return c.db.Close()
}
