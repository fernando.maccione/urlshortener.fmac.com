package redis

import (
	"bytes"
	"encoding/json"
	"log"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"github.com/gomodule/redigo/redis"
	"urlshortener.fmac.com/service/config"
)

type RedisSession interface {
	Ping() (err error)
	Close()
	Write(key string, data interface{}) error
	Read(key string, data interface{}) error
	Delete(key string) error
	ExpiresAt(key string, t time.Time) error
	Exists(key string) bool
}

var initialized uint32
var mu sync.Mutex
var rPool *redisPool

type redisPool struct {
	pool *redis.Pool
}

func GetConnection() RedisSession {
	if !config.GetInstance().TestMode {
		return &redisSessionImp{
			conn: rPool.pool.Get(),
		}
	} else {
		return &redisSessionTESTImp{}
	}
}

type redisSessionImp struct {
	conn redis.Conn
}

func (s *redisSessionImp) Ping() (err error) {
	_, err = s.conn.Do("PING")
	return
}

func (s *redisSessionImp) Close() {
	s.conn.Close()
}

func (s *redisSessionImp) Write(key string, data interface{}) error {

	jsonData := &bytes.Buffer{}
	if err := json.NewEncoder(jsonData).Encode(data); err != nil {
		return err
	}
	if _, err := s.conn.Do("HSET", key, "value", jsonData); err != nil {
		return err
	}
	return nil
}

func (s *redisSessionImp) Read(key string, data interface{}) error {

	reply, err := redis.Values(s.conn.Do("HMGET", key, "value"))
	if err != nil {
		return err
	}

	var jsonData string
	if _, err := redis.Scan(reply, &jsonData); err != nil {
		return err
	}
	return json.NewDecoder(strings.NewReader(jsonData)).Decode(data)
}

func (s *redisSessionImp) ExpiresAt(key string, t time.Time) error {
	_, err := s.conn.Do("EXPIREAT", key, t.Unix())
	if err != nil {
		return err
	}
	return nil
}

func (s *redisSessionImp) Exists(key string) bool {
	v, err := redis.Bool(s.conn.Do("EXISTS", key))
	if err != nil {
		panic(err)
	}
	return v
}

func (s *redisSessionImp) Delete(key string) error {
	_, err := s.conn.Do("DEL", key)
	return err
}

func Init() error {
	if initialized == 0 {
		mu.Lock()
		defer mu.Unlock()
		conf := config.GetInstance()
		host := conf.Redis.Host
		port := conf.Redis.Port
		maxActive := conf.Redis.MaxActive
		maxIdle := conf.Redis.MaxIdle
		idleTimeout := conf.Redis.IdleTimeout

		pool := &redis.Pool{
			MaxActive:   maxActive,
			MaxIdle:     maxIdle,
			IdleTimeout: time.Duration(idleTimeout) * time.Second,
			Dial: func() (redis.Conn, error) {
				return redis.Dial("tcp", host+":"+port)
			},
		}
		rPool = &redisPool{
			pool: pool,
		}
		atomic.StoreUint32(&initialized, 1)
	}
	return testConnection()
}

func testConnection() error {
	s := GetConnection()
	defer s.Close()

	if err := s.Ping(); err != nil {
		return err
	}
	log.Println("Redis connection: OK")
	return nil
}

/***Implementación para TEST*****/
type redisSessionTESTImp struct{}

func (s *redisSessionTESTImp) Ping() (err error) {
	return nil
}
func (s *redisSessionTESTImp) Close() {
}
func (s *redisSessionTESTImp) Write(key string, data interface{}) error {
	return nil
}
func (s *redisSessionTESTImp) Read(key string, data interface{}) error {
	return nil
}
func (s *redisSessionTESTImp) Delete(key string) error {
	return nil
}
func (s *redisSessionTESTImp) ExpiresAt(key string, t time.Time) error {
	return nil
}
func (s *redisSessionTESTImp) Exists(key string) bool {
	return false
}
