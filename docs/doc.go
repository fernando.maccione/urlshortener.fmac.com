// Package classification awesome.
//
// Documentation of our awesome API.
//
//     Schemes: http
//     BasePath: /
//     Version: 1.0.0
//     Host: ec2-54-144-66-165.compute-1.amazonaws.com
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Security:
//     - basic
//
//    SecurityDefinitions:
//    basic:
//      type: basic
//
// swagger:meta
package docs

import (
	"urlshortener.fmac.com/model"
)

// swagger:route POST /admin/api/urlshortener shortener-Admin paramPostRequest
// Create a new url shortener
// responses:
//   200: shortenerResponseWrapper
//	 500:

// swagger:route GET /admin/metric shortener-Metric paramGetMetricRequest
// Metric for prometheus
// responses:
//   200:

// swagger:route GET /admin/api/urlshortener/{UriShortener} shortener-Admin paramGetRequest
// Get a url shortener
// responses:
//   200: shortenerResponseWrapper
//   404:
//	 500:

// swagger:route PUT /admin/api/urlshortener/{UriShortener} shortener-Admin paramPutRequest
// Update a url shortener
// responses:
//   200: shortenerResponseWrapper
//   404:
//	 500:

// swagger:route DELETE /admin/api/urlshortener/{UriShortener} shortener-Admin paramDeleteRequest
// DELETE a url shortener
// responses:
//   200:
//   404:
//	 500:

// swagger:route GET /{UriShortener} shortener-Server paramGetRedirect
// Get a url shortener
// responses:
//   308:

// swagger:parameters paramPostRequest
type paramPostRequest struct {
	// in:body
	Body struct {
		Acitvo    bool   `json:"activo"`
		TargetUrl string `json:"targetUrl"`
		Email     string `json:"email"`
	}
}

// swagger:response shortenerResponseWrapper
type shortenerResponseWrapper struct {
	// in:body
	Body model.ShortenerUrl
}

// swagger:parameters paramGetRequest
type shortenerParamsGetWrapper struct {
	// Code of UriShortener to return
	// in:path
	UriShortener string
}

// swagger:parameters paramGetMetricRequest
type shortenerParamsMetricWrapper struct {
}

// swagger:parameters paramDeleteRequest
type shortenerParamsDeleteWrapper struct {
	// Code of UriShortener to delete
	// in:path
	UriShortener string
}

// swagger:parameters paramPutRequest
type shortenerParamsPutWrapper struct {
	// Code of UriShortener to update
	// in:path
	UriShortener string
	// in:body
	Body model.ShortenerUrl
}

// swagger:response shortenerResponseDeleteWrapper
type shortenerResponseDeleteWrapper struct {
	// in:body
	Body model.ShortenerUrl
}

// swagger:parameters paramGetRedirect
type shortenerParamsRedrirecWrapper struct {
	// Code of UriShortener to redirect
	// in:path
	UriShortener string
}
