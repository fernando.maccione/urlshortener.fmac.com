// Package classification awesome.
//
// It is api for shortener url
//
//     Schemes: http
//     BasePath: /
//     Version: 1.0.0
//     Host:
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Security:
//     - basic
//
//    SecurityDefinitions:
//    basic:
//      type: basic
//
// swagger:meta
package main

import (
	_ "github.com/pdrum/swagger-automation/docs"
	"urlshortener.fmac.com/db/postgresql"
	"urlshortener.fmac.com/db/redis"
	"urlshortener.fmac.com/web"
)

func main() {
	postgresql.GetPostgreSql().Init()
	redis.Init()
	web.Register()
}
