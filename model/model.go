package model

type ShortenerUrl struct {
	Id           int64  `json:"id"`
	Acitvo       bool   `json:"activo"`
	ShortenerUrl string `json:"shortenerUrl"`
	ShortenerUri string `json:"shortenerUri"`
	TargetUrl    string `json:"targetUrl"`
	Email        string `json:"email"`
}
