package cache

import (
	"time"

	"urlshortener.fmac.com/db/redis"
)

type Cache interface {
	Write(key string, data interface{}, expiretAt time.Time) error
	Read(key string, data interface{}) (bool, error)
	Exists(key string) bool
	Delete(key string) error
}

type cacheRedisImp struct {
}

func GetCache() Cache {
	return cacheRedisImp{}
}

func (c cacheRedisImp) getKey(key string) string {
	return `shortener:` + key
}

func (c cacheRedisImp) Write(key string, data interface{}, expiretAt time.Time) error {
	rd := redis.GetConnection()
	defer rd.Close()

	if err := rd.Write(c.getKey(key), data); err != nil {
		return err
	}
	return rd.ExpiresAt(c.getKey(key), expiretAt)
}

func (c cacheRedisImp) Exists(key string) bool {
	rd := redis.GetConnection()
	defer rd.Close()
	return rd.Exists(c.getKey(key))
}

func (c cacheRedisImp) Delete(key string) error {
	rd := redis.GetConnection()
	defer rd.Close()
	return rd.Delete(c.getKey(key))
}

func (c cacheRedisImp) Read(key string, data interface{}) (bool, error) {
	rd := redis.GetConnection()
	defer rd.Close()
	if rd.Exists(c.getKey(key)) {
		return true, rd.Read((c.getKey(key)), data)
	} else {
		return false, nil
	}
}
