package config

import (
	"log"

	confiGIT "github.com/olebedev/config"
)

type postGres struct {
	/*RDS POSTGRES:*/
	DataBase string
	User     string
	Password string
	Port     int
	Host     string
	/*Redis*/

}

type redis struct {
	MaxActive   int
	MaxIdle     int
	IdleTimeout int
	Port        string
	Host        string
}

type Config struct {
	Postgres postGres
	Redis    redis
	TestMode bool
}

var conf *Config

func GetInstance() *Config {
	if conf == nil {
		iniT()
	}

	return conf
}

func iniT() {
	log.Println("Leyendo configuracion...")
	if err := fillConfig(); err != nil {
		log.Fatal("No fue posible leventar el servicio. Ocurrieron errores al leer la configuracion. ", err)
	}
}

/*Nada, por ahora aca, despues se pasa a un file usando algún lib*/
func fillConfig() error {
	cfg, err := confiGIT.ParseYamlFile("server.yml")
	if err != nil {
		return err
	}
	dataBase, _ := cfg.String("database.postgres.dataBase")
	user, _ := cfg.String("database.postgres.user")
	password, _ := cfg.String("database.postgres.password")
	port, _ := cfg.Int("database.postgres.port")
	host, _ := cfg.String("database.postgres.host")

	maxActive, _ := cfg.Int("database.redis.maxActive")
	maxIdle, _ := cfg.Int("database.redis.maxIdle")
	idleTimeout, _ := cfg.Int("database.redis.idleTimeout")
	rport, _ := cfg.String("database.redis.port")
	rhost, _ := cfg.String("database.redis.host")

	conf = &Config{
		Postgres: postGres{
			DataBase: dataBase,
			User:     user,
			Password: password,
			Port:     port,
			Host:     host,
		},
		Redis: redis{MaxActive: maxActive,
			MaxIdle:     maxIdle,
			IdleTimeout: idleTimeout,
			Port:        rport,
			Host:        rhost,
		},

		TestMode: false,
	}
	return nil

}
