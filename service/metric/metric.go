package metric

import (
	"log"
	"net/http"
	"sync"
	"sync/atomic"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	dto "github.com/prometheus/client_model/go"
)

type Metric interface {
	CounterIncrement(conterName string, label1 string, label2 string, value1 string, value2 string)
	GetCounterValue(conterName string, label1 string, label2 string, value1 string, value2 string) float64
	SetGaugeValue(gaugeName string, lavel string, value float64)
	GetGaugeValue(gaugeName string, lavel string) float64
	Handler() http.Handler
	Init()
}

var initialized uint32
var mu sync.Mutex
var instance *ServicePrometheus

const nameSpace = "FMAC"
const subSystem = "SHORTENER"

func GetMetric() Metric {
	if atomic.LoadUint32(&initialized) == 1 {
		return instance
	}
	mu.Lock()
	defer mu.Unlock()
	if initialized == 0 {
		instance = &ServicePrometheus{}
		instance.Init()
	}
	return instance
}

type ServicePrometheus struct {
	cacheIndicadores map[string]*prometheus.CounterVec
	cacheGouges      map[string]*prometheus.GaugeVec
}

func (c *ServicePrometheus) Init() {
	instance.cacheIndicadores = make(map[string]*prometheus.CounterVec)
	instance.cacheGouges = make(map[string]*prometheus.GaugeVec)
	atomic.StoreUint32(&initialized, 1)
	go refreshIndicadores()
}
func (c *ServicePrometheus) CounterIncrement(conterName string, label1 string, label2 string, value1 string, value2 string) {
	indicador := *c.getIndicador(conterName, label1, label2)
	indicador.With(prometheus.Labels{label1: value1, label2: value2}).Inc()

}

func (c *ServicePrometheus) GetCounterValue(conterName string, label1 string, label2 string, value1 string, value2 string) float64 {
	indicador := *c.getIndicador(conterName, label1, label2)

	indicador.WithLabelValues(label1, label2).Inc()
	m := &dto.Metric{}
	if err := indicador.With(prometheus.Labels{label1: value1, label2: value2}).Write(m); err != nil {
		return 0
	}
	return m.Counter.GetValue()
}

func (c *ServicePrometheus) SetGaugeValue(gaugeName string, lavel string, value float64) {
	indicador := *c.getGauge(gaugeName)
	indicador.With(prometheus.Labels{"tag": lavel}).Set(value)
}

func (c *ServicePrometheus) GetGaugeValue(gaugeName string, lavel string) float64 {
	indicador := *c.getGauge(gaugeName)
	m := &dto.Metric{}
	if err := indicador.With(prometheus.Labels{"tag": lavel}).Write(m); err != nil {
		return 0
	}
	return m.Gauge.GetValue()
}

func (c *ServicePrometheus) Handler() http.Handler {
	return promhttp.Handler()
}

func (c *ServicePrometheus) getIndicador(countName string, label1 string, label2 string) *prometheus.CounterVec {
	keyIndicador := c.getKey(nameSpace, subSystem, countName)
	indicador, exist := c.cacheIndicadores[keyIndicador]
	if exist {
		return indicador
	} else {
		mu.Lock()
		defer mu.Unlock()
		indicador, exist = c.cacheIndicadores[keyIndicador]
		if !exist { //is ya existe, es porque justo la petición anterior que bloqueó el proceso la creo entonces devuevlo esa
			indicadoR := promauto.NewCounterVec(prometheus.CounterOpts{
				Name:      countName,
				Help:      "",
				Namespace: nameSpace,
				Subsystem: subSystem,
			}, []string{label1, label2})
			c.cacheIndicadores[keyIndicador] = indicadoR
			return indicadoR
		} else {
			return indicador
		}
	}
}

func (c *ServicePrometheus) getGauge(gaugeName string) *prometheus.GaugeVec {
	keyIndicador := c.getKey(nameSpace, gaugeName, "")
	indicador, exist := c.cacheGouges[keyIndicador]
	if exist {
		return indicador
	} else {
		mu.Lock()
		defer mu.Unlock()
		indicador, exist = c.cacheGouges[keyIndicador]
		if !exist { //is ya existe, es porque justo la petición anterior que bloqueó el proceso la creo entonces devuevlo esa
			indicadoR := promauto.NewGaugeVec(prometheus.GaugeOpts{
				Name:      gaugeName,
				Help:      "",
				Namespace: nameSpace,
				Subsystem: subSystem,
			}, []string{"tag"})
			c.cacheGouges[keyIndicador] = indicadoR
			return indicadoR
		} else {
			return indicador
		}
	}
}

func (c *ServicePrometheus) getKey(space string, system string, name string) string {
	key := space + "_" + system + "_" + name
	return key
}

func (c *ServicePrometheus) cleanContadores() {
	for _, indicador := range c.cacheIndicadores {
		indicador.Reset()
	}
}

func refreshIndicadores() {
	for {
		now := time.Now()
		tomorrow := now.Add(time.Hour * 24)
		initDay := time.Date(tomorrow.Year(), tomorrow.Month(), tomorrow.Day(), 0, 0, 0, 0, tomorrow.Location())
		d := initDay.Sub(time.Now())

		time.Sleep(d)
		log.Print("clean counters...")
		instance.cleanContadores()
	}
}
