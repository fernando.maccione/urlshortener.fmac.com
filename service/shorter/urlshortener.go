package shortener

import (
	"errors"
	"log"
	"math"
	"strings"
	"time"

	"urlshortener.fmac.com/dao"
	"urlshortener.fmac.com/model"
	"urlshortener.fmac.com/service/cache"
)

const base62 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

type Shortener interface {
	Update(shortUrl string, dto *model.ShortenerUrl) error
	CreateShortener(dto *model.ShortenerUrl) (*model.ShortenerUrl, error)
	FindByCode(string) (*model.ShortenerUrl, error)
	DeleteByShortenerUrl(code string) error
	GetTargetUrl(shortUrl string) (*model.ShortenerUrl, error)
}

func GetInstance() Shortener {
	return &shortenerImpl{}
}

type shortenerImpl struct{}

func (c shortenerImpl) Update(shortUrl string, dto *model.ShortenerUrl) error {
	if err := c.Check(shortUrl, dto); err != nil {
		return err
	}
	if err := dao.GetShortenerDao().Update(dto); err != nil {
		return err
	}
	putOnCache(dto)
	return nil
}

func (c shortenerImpl) CreateShortener(dto *model.ShortenerUrl) (*model.ShortenerUrl, error) {
	shortener, err := dao.GetShortenerDao().Insert(dto)
	if err != nil {
		return nil, err
	}
	shortener.ShortenerUri = EncodeShortUrl(shortener.Id)

	if err := dao.GetShortenerDao().Update(dto); err != nil {
		return nil, err
	}
	putOnCache(dto)
	return shortener, nil
}

func (c shortenerImpl) FindByCode(code string) (*model.ShortenerUrl, error) {
	dto := getFromCache(code)
	if dto != nil {
		return dto, nil
	} else {
		dto, err := dao.GetShortenerDao().FindByCode(code)
		if err != nil {
			return nil, err
		} else {
			if dto != nil {
				putOnCache(dto)
			}
			return dto, err
		}

	}
}

func (c shortenerImpl) DeleteByShortenerUrl(code string) error {
	removeCache(code)
	return dao.GetShortenerDao().Delete(code)
}

func (c shortenerImpl) GetTargetUrl(shortUrl string) (*model.ShortenerUrl, error) {
	su, err := c.FindByCode(shortUrl)
	if err != nil {
		return nil, err
	}
	if su != nil && su.Acitvo {
		return su, nil
	} else {
		return nil, nil
	}
}

func (c shortenerImpl) Check(codigo string, dto *model.ShortenerUrl) error {
	if codigo != dto.ShortenerUri {
		return errors.New("no está permitido cambiar la url corta")
	}
	id, err := DecodeShortUrl(dto.ShortenerUri)
	if err != nil {
		return errors.New("la url corta es incorrecta")
	}
	if id != dto.Id {
		return errors.New("no está permitido modifica el id o la url corta")
	}
	return nil
}
func getFromCache(key string) *model.ShortenerUrl {

	var dto model.ShortenerUrl
	exit, err := cache.GetCache().Read(key, &dto)
	if err != nil {
		log.Print("Error read from cache.", err)
		return nil
	} else if exit {
		return &dto
	}
	return nil
}

func removeCache(key string) {
	err := cache.GetCache().Delete(key)
	if err != nil {
		log.Print("Error delete in cache.", err)
	}
}
func putOnCache(dto *model.ShortenerUrl) {
	err := cache.GetCache().Write(dto.ShortenerUri, dto, time.Now().Add(time.Hour*24))
	if err != nil {
		log.Print("Error write in cache.", err)
	}
}

func EncodeShortUrl(key int64) string {
	base := int64(len(base62))
	var shortUrl string
	for key > 0 {
		pos := key % base
		key /= base
		shortUrl = string(base62[pos]) + shortUrl
	}
	return shortUrl
}

func DecodeShortUrl(shortUrl string) (int64, error) {
	var val int64
	base := int64(len(base62))
	for index, char := range shortUrl {
		pow := len(shortUrl) - (index + 1)
		pos := strings.IndexRune(base62, char)
		if pos == -1 {
			return 0, errors.New("invalid character: " + string(char))
		}

		val += int64(pos) * int64(math.Pow(float64(base), float64(pow)))
	}

	return val, nil
}
