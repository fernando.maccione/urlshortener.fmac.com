module urlshortener.fmac.com

go 1.16

require (
	github.com/felixge/httpsnoop v1.0.2 // indirect
	github.com/fsnotify/fsnotify v1.5.1 // indirect
	github.com/gin-contrib/sse v0.1.1-0.20210824015140-ab64730e3795 // indirect
	github.com/gin-gonic/gin v1.7.2-0.20210831015854-30cdbfcf4c90
	github.com/go-openapi/analysis v0.20.1 // indirect
	github.com/go-openapi/errors v0.20.1 // indirect
	github.com/go-openapi/jsonreference v0.19.6 // indirect
	github.com/go-openapi/runtime v0.19.31 // indirect
	github.com/go-openapi/strfmt v0.20.2 // indirect
	github.com/go-stack/stack v1.8.1 // indirect
	github.com/go-swagger/go-swagger v0.27.0 // indirect
	github.com/goccy/go-json v0.7.8 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/gomodule/redigo v1.8.5
	github.com/json-iterator/go v1.1.12-0.20210722010026-c6661824eb80 // indirect
	github.com/lib/pq v1.10.3
	github.com/mattn/go-isatty v0.0.14-0.20210829144114-504425e14f74 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2-0.20210109003243-333559e1834b // indirect
	github.com/olebedev/config v0.0.0-20190528211619-364964f3a8e4
	github.com/pdrum/swagger-automation v0.0.0-20190629163613-c8c7c80ba858
	github.com/pelletier/go-toml v1.9.4 // indirect
	github.com/prometheus/client_golang v1.11.1-0.20210812155644-2261d5cda14e
	github.com/prometheus/client_model v0.2.0
	github.com/prometheus/procfs v0.7.4-0.20210830210623-21d221eae0ff // indirect
	github.com/spf13/cast v1.4.1 // indirect
	github.com/spf13/viper v1.8.1 // indirect
	github.com/ugorji/go v1.2.6 // indirect
	go.mongodb.org/mongo-driver v1.7.2 // indirect
	golang.org/x/mod v0.5.0 // indirect
	golang.org/x/net v0.0.0-20210907225631-ff17edfbf26d // indirect
	golang.org/x/sys v0.0.0-20210906170528-6f6e22806c34 // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.5 // indirect
	google.golang.org/protobuf v1.27.2-0.20210806184350-5aec41b4809b // indirect
	gopkg.in/ini.v1 v1.63.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
